# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=xmrig
pkgver=6.14.0
pkgrel=0
pkgdesc="XMRig is a high performance Monero (XMR) miner"
url="https://xmrig.com/"
arch="aarch64 x86 x86_64" # officially supported by upstream
license="GPL-3.0-or-later"
options="!check" # No test suite from upstream
makedepends="cmake libmicrohttpd-dev libuv-dev openssl-dev hwloc-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/xmrig/xmrig/archive/v$pkgver.tar.gz
	enable-donateless-mode.patch
	fix-narrowing-conversion.patch
	"

case "$CARCH" in
	aarch64*) CMAKE_CROSSOPTS="-DARM_TARGET=8"; makedepends="$makedepends linux-headers" ;;
esac

build() {
	cmake -B build $CMAKE_CROSSOPTS -DCMAKE_BUILD_TYPE=None \
		-DWITH_CUDA=OFF \
		-DWITH_OPENCL=OFF

	make -C build
}

package() {
	install -Dm 755 build/xmrig $pkgdir/usr/bin/xmrig

	install -Dm 644 -t "$pkgdir"/usr/share/doc/$pkgname/ README.md
}

sha512sums="
de1cc3543a690a65cb50e629114324e95d4769dc3610c6dde0fff937206df3949907f8a21a4e4533bee71958e6febbaf4a23c3092c1e67a763f4ffea11770591  xmrig-6.14.0.tar.gz
40cd7e3a884920951ec48efebbea5d7181deaeef6a226444a46ad8dc83b54eceae954c8d28952c21d63a15a4947eac72d1024b83684b5cb15437d3c8d32b006a  enable-donateless-mode.patch
c11effaa9fb199e92b9e2f126b5e469bfab11053704ccbf5bcc79d1a554dccb849bd5a8579cbe707180fd62b42732fdc56ece2361201440c6d32c39189bf1e4a  fix-narrowing-conversion.patch
"
