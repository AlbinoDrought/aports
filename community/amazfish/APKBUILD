# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=amazfish
pkgver=2.0.1
pkgrel=0
_commit_qble="2c287455387892e9158c108679921eaf0581a7fa"
_commit_libwatchfish="ec302cd25d4605e56148a750f2f75fb5c641cfee"
pkgdesc="Companion application for Huami Devices (such as Amazfit Bip, Cor, MiBand2/3 and GTS and GTS) and the Pinetime Infinitime"
url="https://github.com/piggz/harbour-amazfish"
# armhf blocked by kdb
arch="all !armhf"
license="AGPL-3.0-only AND GPL-3.0-only"
depends="
	kdb-sqlite
	kirigami2
	nemo-qml-plugin-dbus
	"
makedepends="
	kdb-dev
	qt5-qtbase-dev
	qt5-qtconnectivity-dev
	qt5-qtlocation-dev
	qtmpris-dev
	"
source="https://github.com/piggz/harbour-amazfish/archive/$pkgver/harbour-amazfish-$pkgver.tar.gz
	https://github.com/piggz/qble/archive/$_commit_qble/qble-$_commit_qble.tar.gz
	https://github.com/piggz/libwatchfish/archive/$_commit_libwatchfish/libwatchfish-$_commit_libwatchfish.tar.gz
	amazfish-launcher.sh
	amazfish.desktop
	"
options="!check" # No tests
builddir="$srcdir/harbour-amazfish-$pkgver"

prepare() {
	default_prepare

	rmdir qble daemon/libwatchfish
	mv "$srcdir/qble-$_commit_qble" qble
	mv "$srcdir/libwatchfish-$_commit_libwatchfish" daemon/libwatchfish
}

build() {
	qmake-qt5 \
		PREFIX=/usr \
		FLAVOR=kirigami
	make
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	install -Dm644 "$srcdir"/amazfish.desktop -t "$pkgdir"/etc/xdg/autostart/
	install -Dm755 "$srcdir"/amazfish-launcher.sh "$pkgdir"/usr/libexec/amazfish-launcher

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
f71f64fa2eb832cfbf1b56ff0338cf0f25054ce028754ecf2f02a6d22ff34a421651bf39f5e0f869d713eeeed4852ca4cbe154761a232e928e2250ee92a6eadf  harbour-amazfish-2.0.1.tar.gz
55207138b317b30b108b15d3fe07b4c5a2ef959ef707930bffa5339ce846308e264c791bde0c7d7839d61966463bfd91d894ba473f6e03537b901bc74d3a51b1  qble-2c287455387892e9158c108679921eaf0581a7fa.tar.gz
79ddcc7b68b78a5dcaab1c8efe10a623af198ec9c71bc3a971fe9baa267d18b0bc6b538c7ed1c41bfcdf86dc5c21fc5bfd25b0e976f0e82d45e88ec8636b6368  libwatchfish-ec302cd25d4605e56148a750f2f75fb5c641cfee.tar.gz
a17c0d6578e0d6878099f9c913e54100c44dbb94cf8803f2780d5709ec08136daa832ec2ffe947fb8a91e02320f01041d0e763bcc08350270af36d89f767ca14  amazfish-launcher.sh
930f2cae5f88559a83dd46d11d2161a9239efdd46ad6b91dc530eb4a7863f197a7865f9599973b71bcc7d1e2346c848ea7b9a57f5b714560d101b0f384c0f4d1  amazfish.desktop
"
